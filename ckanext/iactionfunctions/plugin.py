import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.lib.dictization.model_dictize as model_dictize
from ckan.logic.action.update import package_update
from ckan.logic.action.create import package_create
from copy import deepcopy


def my_package_update(context, data_dict):
    model = context['model']
    # print('DATA DICT', data_dict)
    name_or_id = data_dict.get("id") or data_dict['name']
    # import pdb; pdb.set_trace()
    pkg = model.Package.get(name_or_id)
    # print('pkg', pkg)
    package = model_dictize.package_dictize(pkg, context)  
    if data_dict['version'] != pkg.version:
        old_vers = '-%s' % pkg.version.replace('.', '-')
        new_vers = '-%s' % data_dict['version'].replace('.', '-')
        if data_dict['name'].endswith(old_vers):
            to_replace = data_dict['name'][len(data_dict['name'])-len(old_vers):]
            data_dict['name'] = data_dict['name'].replace(to_replace, new_vers) 
        else:
            data_dict['name'] = data_dict['name'] + new_vers
        base_name = data_dict['name'][:-len(new_vers)] + '-base'
        data_dict['id'] = data_dict['name']
        if 'resources' in package:                                      
            data_dict['resources'] = deepcopy(package['resources'])
            data_dict['num_resources'] = len(data_dict['resources'])
        dataset = toolkit.get_action('package_create')(context, deepcopy(data_dict)) 
        toolkit.get_action('dataset_version_create')(
            context,
            {
                'base_name': base_name,
                'id': data_dict['id'],
                'owner_org': data_dict['owner_org']
            }
        )
        output = toolkit.get_action('package_show')(context, dataset)
       # if 'resources' in package:                                  
       #     output['resources'] = deepcopy(package['resources'])
       #     output['num_resources'] = len(output['resources'])
        print('OUTPUT', output)

    else:
        if ('version' not in data_dict) or (not  data_dict['name'].endswith('-%s' % data_dict['version'])):
            base_name = data_dict['name'] + '-base'
        else:
            base_name = data_dict['name'][:-len('-%s' % data_dict['version'])] + '-base'
        base_pkg = model.Package.get(base_name)
        # print('PKG_OLD', base_pkg)
        if base_pkg is None:
            toolkit.get_action('dataset_version_create')(
                context,
                {
                    'base_name': base_name,
                    'id': data_dict['id'],
                    'owner_org': data_dict['owner_org']
                }
            )
        output = package_update(context, data_dict)
    return output

#def my_package_create(context, data_dict):
#    if ('version' not in data_dict) or (data_dict['version'] == ''):
#        data_dict['version'] = '1'
#    dataset = package_create(context, data_dict)  
#    if '-base' not in dataset['name']:
#        base_name = dataset['name'] + '-base' if '-base' not in dataset['name'] else dataset['name']
#        toolkit.get_action('dataset_version_create')(    
#            context,
#            {
#                'base_name': base_name,
#                'id': dataset['id'],
#                'owner_org': data_dict['owner_org']
#            }
#        )
#    return dataset
    


class IactionfunctionsPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IActions)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'iactionfunctions')

    # IActions

    def get_actions(self):
        return {
            'package_update': my_package_update,
            # 'package_create': my_package_create
        }
